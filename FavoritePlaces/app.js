﻿'use strict';


var mapBasePath = 'https://www.google.com/maps/embed/v1/place';
var mapBase = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCZ_ZZWJ6ZcidQODLuhSmzPYgJgKUtjxRY&zoom=14&q=';
var geoCodePath = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
var apiKey = 'AIzaSyCZ_ZZWJ6ZcidQODLuhSmzPYgJgKUtjxRY';


var favApp = angular.module('favPlaces', [
  'ngRoute'
]);
/*
favApp.config(
    function ($routeProvider) {
        $routeProvider
        .when('/home', {
            templateUrl: 'Views/home.html',
            controller: 'PlacesController'
        })
            .when('/addPlace', {
                templateUrl: 'Views/addForm.html',
                controller: 'PlacesController'
            })
           // .otherwise({ redirectTo: '/' });
    }
);
*/
favApp.filter('trusted', ['$sce', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);

// favApp.directive('');



