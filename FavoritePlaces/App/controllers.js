﻿var favApp = angular.module('favPlaces');

favApp.controller('PlacesController', ['$scope', 'appService', function ($scope, appService) {
    $scope.places = []; // all of our places
    $scope.url = "";    // url for individual location 
    $scope.urlAllLocations = ""; //
    $scope.currentPlace = {}; //  
    // $scope.clear = { name: "", zipCode: "" };
    var self = this;


    // Mock data 

    $scope.places = [
        {
            name: "Home",
            zipCode: 46032,
            description: "Here's home."
        },
        {
            name: "Vacation spot",
            zipCode: 81302,
            description: "The perfect getaway(for me)."
        }
    ];
    /**/

    $scope.AddLocation = function () {
        $scope.showAddPlaceForm = true;
    };

    $scope.saveForm = function (place) {
        if (appService.validatePlace(place)) {
            $scope.places.push({
                name: place.name,
                zipCode: place.zipCode,
                description: place.description
            });
            this.clearInputForm(place);
            $scope.showAddPlaceForm = false;
        }
    };


    $scope.cancelForm = function (place) {
        this.clearInputForm(place);
        $scope.showAddPlaceForm = false;
    };

    $scope.clearInputForm = function (place) {
        if (place) {
            place.name = '';
            place.zipCode = '';
            place.description = '';
        }
    };

    $scope.showMap = function (place) {
        $scope.currentPlace = place;
        appService.getLocation(place.zipCode, mapBase, function (url) {
            $scope.url = url; console.log('url= ', url);
        });
    };

    $scope.deleteLocation = function (place) {
        for (var i = 0; i < $scope.places.length; i++) {
            console.log(i + ')' + $scope.places[i].name);
            if ($scope.places[i].name === place.name) {
                $scope.places.splice(i, 1);
            }
        }
    };

}]); // End PlacesController

favApp.controller('ModalController', ['$scope', function ($scope) {
    $scope.saveDescription = function (place) {
    }
}]);