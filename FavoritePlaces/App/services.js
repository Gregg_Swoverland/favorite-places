﻿var favApp = angular.module('favPlaces');

favApp.service('appService', ['$http', function Service($http) {
    var self = this;

    self.getLocation = function (zip, mapBase, callback) {
        $http.get(geoCodePath + zip + '&sensor=true')
        .success(function (response) {
            var coords = response.results[0].geometry.location;
            var url = mapBase + coords.lat + ',' + coords.lng;
            callback(url);
        });
    };

    self.validatePlace = function (place) {
        return ((place.name !== "") && (place.zipCode !== ""));
    }

}]);